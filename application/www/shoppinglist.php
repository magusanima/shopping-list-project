<?php
// Open database connection
$servername = "slp-mysql";
$username = "root";
$password = "password";
$dbname = "slp";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error)
{
    die("Connection failed: " . $conn->connect_error);
}

// Delete last saved iteration on link click
if($_GET["delete"] || $_GET["show"])
{
    $conn->query("DELETE FROM saved_iteration;");
}

// Processing for storing recipe info
if($_POST)
{
    // Empty the database before insertion
    $conn->query("DELETE FROM saved_iteration;");

    $sql = "INSERT INTO saved_iteration (recipes, checked_ingreds)
            VALUES ('".json_encode($_POST['recipes'])."', '".json_encode(array_keys($_POST['ingredient']))."')";

    $result = $conn->query($sql);
}

// Pull POSTed data from saved_iteration database table on page load
$sql = "SELECT saved_iteration.recipes,
        saved_iteration.checked_ingreds
        from saved_iteration";
$result = $conn->query($sql);
$stored_data = $result->fetch_assoc();

// SQL query to randomly select 30 recipes and place related data into array
$sql = "SELECT distinct recipes.recipe_name, 
        recipes.id,
        recipe_type.food_type,
        recipe_type.type_max,
        recipes.servings, 
        recipes.reselection_flag
        from recipes
        JOIN recipe_type ON recipe_type.type_id = recipes.recipe_food_type
        ". // If there is an existing saved iteration load that, if not continue
           // implode() converts from an array to a string
           // json.decode needed to 'decode' previously stored encoded data
           // 'true' sets output to be an array rather than an object (default)
        ($stored_data ? "WHERE recipes.id in (".implode(",", json_decode($stored_data["recipes"], true)).")
        order by FIELD(recipes.id, ".implode(",", json_decode($stored_data["recipes"], true)).")" : "order by rand() limit 30");
           // order by the order of the elements that were passed in (keeps the order of recipes the same on reload)

$result = $conn->query($sql);

// Create array to hold all recipe data
$recipe_info_main = [];

// Counter array for each food type used to check no more than 1 (soup) or 2 items selected from each category
$food_type_counter = [];

// Run checks on and return data
if($result)
{
    // while loop needed to retrieve more than one row of data
    while($row = $result->fetch_assoc())
    {
        // Use php to run checks on data - want to grab 8 recipes - eg. no more than 1 soup, no more than 2 salads etc.
        // "($_GET["show"] ?: 8))" for changing modes between 7 day and 4 day
        if($food_type_counter[$row["food_type"]] < $row["type_max"] && count($recipe_info_main) < ($_GET["show"] ?: 8))
        {
            $recipe_info_main[] = $row;
            $food_type_counter[$row["food_type"]]++;
        }
    }
}

// Another SQL query to retrieve ingredient data relating to each recipe selected
$ingredients_list = [];
// '$menu_list' is an array containing all the data relating to a single recipe
// foreach loops through every iteration of a recipe
foreach ($recipe_info_main as $index => $menu_list)
{
    $sql = "SELECT ingredients.ingredient,
            recipe_ingredients.ingredient_quantity,
            recipe_ingredients.ingredient_units,
            ingredients.category,
            ingredients.id AS ingredient_id
            from recipe_ingredients
            JOIN ingredients ON recipe_ingredients.ingredients_id = ingredients.id
            WHERE recipe_ingredients.recipes_id = {$menu_list["id"]}";
    $result = $conn->query($sql);
    if($result)
    {
        // Division required to adjust ingredient quantity based on serving sizes (default 4)
        // If /$index is entry 6 or 7 (Sunday Lunch/Dinner) serving size needs to be 2 , if not serving size is 4
        $ingredient_divider = ($index >= 6 ? 2 : 4) / $menu_list["servings"];
        while($row = $result->fetch_assoc())
        {
            // Pull ingredient info into new array
            // Adding together duplicate entries of ingredient items and adjusting the quantity based on serving size (4 serving default)
            // Grouping ingredients into categories with [$row["category"]
            $current_ingredient = &$ingredients_list[$row["category"]][$row["ingredient_id"]];
            if($current_ingredient)
            {
                $current_ingredient["ingredient_quantity"] += ($row["ingredient_quantity"] * $ingredient_divider);
            }
            // Adding new ingredient entry to array if it is the first instance if it and adjusting the quantity based on serving size (4 serving default)
            else
            {
                $current_ingredient = $row;
                $current_ingredient["ingredient_quantity"] = ($row["ingredient_quantity"] * $ingredient_divider);
            }
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="stylesheet.css">
    <title>Shopping List Generator</title>
</head>

<body>
    <!--  Wrapped in a form in order to save the currently generated selection for later  -->
    <form action="#" method="POST">
        <!--  BUTTON MENU  -->
        <div class="p-3 container d-flex">
            <div class="p-2">
                <!--      Save this iteration for later on button click      -->
                <input type="submit" class="btn btn-success" name="Submit" value="Save this Selection">
            </div>
            <div class="p-2">
                <!--      Delete last saved iteration and reset page on link click      -->
                <a href="?delete=1" class="btn btn-danger">Finished/Reset</a>
            </div>
            <div class="p-2">
                <!--      Swap to 4 day mode      -->
                <a href="?show=4" class="btn btn-primary">Swap Mode to 4 Day</a>
            </div>
        </div>

        <!-- MENU TABLE (using bootstrap) -->
        <div class="container mt-3">
            <h2>Menu</h2>
            <table class="table table-bordered align-middle">
                <thead>
                <tr>
                    <th class="day_data"></th>
                    <th class="table_header">Lunch</th>
                    <th class="table_header">Dinner</th>
                </tr>
                </thead>
                <tbody>
                <?php
                    // Array with day data for table
                    $days = ["M", "T", "W", "T", "F", "S", "S"];

                    // Loop to create table layout
                    // "(count($recipe_info_main) == 8 ? 7 : 4)" depending on number of recipes in array change table to display 7 or 4 days data
                    for ($x = 0; $x < (count($recipe_info_main) == 8 ? 7 : 4); $x++)
                    {
                        echo " 
                    <tr>
                        <th class='table_header'>{$days[$x]}</th>
                        <!-- Use rowspan to create correct layout in table -->
                        <!-- \$x + 1 needed to grab the odd numbered row entries -->
                        <!-- Monday - Saturday -->
                        ".(in_array($x, [0, 2, 4]) ? '<td rowspan ="2">'.$recipe_info_main[$x]["recipe_name"].' x2 </td>' : '')."
                        ".(in_array($x, [0, 2, 4]) ? '<td rowspan ="2">'.$recipe_info_main[$x + 1]["recipe_name"].' x2 </td>' : '')."
                        <!-- Sunday -->
                        ".(in_array($x, [6]) ? '<td>'.$recipe_info_main[$x]["recipe_name"].'</td>' : '')."
                        ".(in_array($x, [6]) ? '<td>'.$recipe_info_main[$x + 1]["recipe_name"].'</td>' : '')."
                    </tr>";
                    }
                ?>
                </tbody>
            </table>
        </div>

        <?php
        //  Loop through $recipe_info_main to add hidden inputs for POSTing data
            foreach($recipe_info_main as $recipe)
            {
                echo
                "<input type='hidden' name='recipes[]' value='{$recipe['id']}'/>";
            }
        ?>

        <br>
        <br>

    <!--    SHOPPING LIST with ingredient totals and checkboxes (using bootstrap)-->
        <div class='container mt-3'>
            <h2>Shopping List</h2>
            <ul class='list-group'>
                <?php
                foreach($ingredients_list as $ingred_info)
                    {
                        foreach($ingred_info as $ingredient)
                        {
                            echo
                            "<li class='list-group-item d-flex justify-content-between align-items-center'>
                            <!-- Ingredient Quantity -->
                            <span class='badge bg-primary rounded-pill'>".round($ingredient["ingredient_quantity"])." {$ingredient["ingredient_units"]}</span>
                            <!-- Ingredient Name -->
                            {$ingredient["ingredient"]}
                            <input class='form-check-input' type='checkbox' id='checkboxNoLabel' value='' name='ingredient[{$ingredient["ingredient_id"]}]'
                            ".
                            // Decoding the encoded stored data and converting to an array
                            // in_array used to check if the ingredient currently being looked at exists in the saved data (if the box has already been checked)
                            (in_array($ingredient["ingredient_id"], json_decode($stored_data["checked_ingreds"], true)) ? "checked" : "").">
                            </li>";
                        }
                    }
                ?>
            </ul>
        </div>
        <br>
        <br>
        <br>
    </form>
</body>

<?php
// Close database connection
$conn->close();

?>