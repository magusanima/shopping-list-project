# Shopping List Generator

This repo contains code for a shopping list generator web application. 
It connects to a MySQL database, retrieves recipe and ingredient data, and allows users to select recipes and generate a shopping list based on the selected items.

Here is a breakdown of what the code does:

1. **Database Connection:** It establishes a connection to a MySQL database using the provided server credentials.

2. **Deleting Saved Iteration:** If the URL contains a "delete" parameter or a "show" parameter, the code deletes the last saved iteration from the "saved_iteration" table in the database.

3. **Storing Recipe Info:** If the HTTP request method is POST, the code stores the selected recipes and checked ingredients in the "saved_iteration" table in the database.

4. **Retrieving Stored Data:** The code retrieves the previously stored recipe and ingredient data from the "saved_iteration" table in the database.

5. **Retrieving Recipe Data:** The code retrieves recipe data from the "recipes" and "recipe_type" tables in the database. The retrieved data includes recipe names, IDs, food types, maximum number of servings, and reselection flags.

6. **Checking and Filtering Recipes:** The code checks the retrieved recipe data to ensure that the selected recipes meet certain criteria, such as not exceeding the maximum number of recipes per food type. It also limits the number of displayed recipes based on the "show" parameter in the URL.

7. **Retrieving Ingredient Data:** For each selected recipe, the code retrieves ingredient data from the "recipe_ingredients" and "ingredients" tables in the database. It calculates the adjusted ingredient quantity based on the number of servings and stores the ingredient data in the `$ingredients_list` array.

8. **HTML Output:** The code generates HTML markup for the web page, including buttons to save, delete, and switch modes, a table displaying the selected recipes, and a shopping list with ingredient quantities and checkboxes.

9. **Form Submission:** The code includes hidden input fields for the selected recipes, allowing the data to be submitted in a form when saving the selection.

10. **Database Connection Closure:** The database connection is closed at the end of the script to free up resources.

Overall, the code provides a user interface for selecting recipes, generating a menu, and creating a shopping list based on the selected recipes and ingredients.